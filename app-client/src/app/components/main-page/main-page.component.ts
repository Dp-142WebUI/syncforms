import {Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {HttpService} from '../../forms/services/http.service';
import {Form} from '../../forms/services/form';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],

  providers: [],
})
export class MainPageComponent implements OnInit {
  public forms: Form[];
  displayedColumns = ['author', 'groups', 'title'];
  formsSource: MatTableDataSource < object > ;
  selectedRowIndex = -1; 
  mode = new FormControl('over');
  
constructor(private http: HttpService) { }

highlight(row) { 
  this.selectedRowIndex = row.title; 
}

initDataSource(data): MatTableDataSource < object > {
  let dataSource = new MatTableDataSource(data);
  return dataSource;
}

renderUsersList() {
  this.formsSource = this.initDataSource(this.forms);
}


ngOnInit() {
  this.http.getAllForms()
    .subscribe(data => {
        this.forms = data;
        this.renderUsersList();
        console.log(this.formsSource);
    });
}
}


//https://medium.com/codingthesmartway-com-blog/angular-material-part-4-data-table-23874582f23a