import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private url = '/';
  private socket = io.connect(this.url);

  constructor() { }

  public sendClick(eventInfo) {
    this.socket.emit('click', eventInfo);
  }

  public sendMouseMovement(eventInfo) {
    this.socket.emit('mouseMove', eventInfo);
  }

  public sendFocusEvent(eventInfo) {
    this.socket.emit('focusEvent', eventInfo);
  }
  
  public sendKeysEvent(eventInfo) {
    this.socket.emit('keyPress', eventInfo);
  }

  public sendOnChangetEvent(eventInfo) {
    this.socket.emit('onChange', eventInfo);
  }

  public getClickEvent() {
    const eventObservable = new Observable(observer => {
      this.socket.on('mouseClick', (eventData) => {
        observer.next(eventData);
      });

      return () => {
        this.socket.disconnect();
      };
    });

    return eventObservable.pipe(debounceTime(100)); 
  }

  public getMouseMove() {
    const eventObservable = new Observable(observer => {
      this.socket.on('newMouseMove', (eventData) => {
        observer.next(eventData);
      });

      return () => {
        this.socket.disconnect();
      };
    });

    return eventObservable;
  }

  public getFocusEvent() {
    const eventObservable = new Observable(observer => {
      this.socket.on('newFocus', (eventData) => {
        observer.next(eventData);
      });

      return () => {
        this.socket.disconnect();
      };
    });

    return eventObservable;
  }

  public getKeyboardEvent() {
    const eventObservable = new Observable(observer => {
      this.socket.on('newKeyPress', (eventData) => {
        observer.next(eventData);
      });

      return () => {
        this.socket.disconnect();
      };
    });

    return eventObservable;
  }

  public getOnChangeEvent() {
    const eventObservable = new Observable(observer => {
      this.socket.on('newOnChange', (eventData) => {
        observer.next(eventData);
      });

      return () => {
        this.socket.disconnect();
      };
    });

    return eventObservable;
  }
}
